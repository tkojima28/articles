.PHONY = server

THEME := angels-ladder

server:
	hugo server -t $(THEME) --buildDrafts

build:
	hugo -t $(THEME) 
